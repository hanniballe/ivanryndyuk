# Ivan Ryndyuk - This is my flutter sotry...

It's a source code of my resume app built with Flutter, so you can get to know me as a developer not only by word, but also by code.

## Original apps

[Ivan Ryndyuk (iOS)](https://itunes.apple.com/app/id1446183650)
[Ivan Ryndyuk (Android)](https://play.google.com/store/apps/details?id=com.ryndyuk.ivanryndyuk)

## For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
