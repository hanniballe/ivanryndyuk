import 'package:url_launcher/url_launcher.dart';

/// URL launcher helper

class Launcher {
  static openURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
