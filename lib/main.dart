import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'timeline_items.dart';
import 'launcher.dart';

void main() => runApp(MyApp());

LayoutHelper layoutHelper;
Map<String, Passage> passages;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ivan Ryndyuk',
      theme: ThemeData(
        primaryColor: Color(0xFFEBEBEB)
      ),
      home: MyStoryPage(),
    );
  }
}

class MyStoryPage extends StatelessWidget {
  final scrollOffset = ValueNotifier(0.0);
  final currentPage = ValueNotifier(0);

  @override
  Widget build(BuildContext context) {
    layoutHelper = LayoutHelper(query: MediaQuery.of(context));

    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection("passages").snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return buildError(snapshot.error.toString(), context);
          } else if (snapshot.hasData) {
            processPassages(snapshot.data.documents);

            if (passages.length == PassageKeys.count) {
              return buildMyStoryPage(context);
            } else {
              return buildError(
                  "We've got some problems retrieving app data...\nPlease check your connection status.",
                  context);
            }
          } else {
            return buildLoading(context);
          }
        });
  }

  void processPassages(List<DocumentSnapshot> docs) {
    var result = Map<String, Passage>();

    docs.forEach((doc) {
      final key = doc.documentID;
      result[key] = Passage(doc);
    });

    passages = result;
  }

  Widget buildLoading(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(),
            ),
            Material(
              color: Colors.transparent,
              child: Text("Loading app data...",
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: layoutHelper.fontSize(k: 0.8),
                      fontStyle: FontStyle.italic)),
            ),
            Container(
              height: layoutHelper.defaultPaddingTrippleValue,
            ),
            CircularProgressIndicator(),
            Expanded(
              flex: 1,
              child: Container(),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildError(String error, BuildContext context) {
    return Container(
      padding: layoutHelper.getDefaultPadding(),
      color: Colors.white,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: Text(error,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: layoutHelper.fontSize(),
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center),
        ),
      ),
    );
  }

  Widget buildMyStoryPage(BuildContext context) {
    var pageController = PageController(initialPage: -1);

    pageController.addListener(() {
      scrollOffset.value = pageController.offset;
    });

    var timelineChildren = List<Widget>();
    final query = MediaQuery.of(context);
    final sw = query.size.width;
    final lineWidth = 1.0;
    final space = (sw - 11.0) / 11.0;
    final padding = query.padding;

    for (int i = 0; i <= 11; i++) {
      if (i == 11 || i == 0) {
        timelineChildren.add(Container(width: space / 2.0));
      } else {
        timelineChildren.add(Container(width: space));
      }

      if (i < 11) {
        timelineChildren.add(Container(
          width: lineWidth,
          height: 16.0,
          color: Colors.black.withOpacity(0.5),
        ));
      }
    }

    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFFFFFFF),
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0.0,
            right: 0.0,
            bottom: 0.0,
            left: 0.0,
            child: PageView.builder(
                itemCount: 10,
                controller: pageController,
                onPageChanged: (page) {
                  currentPage.value = page;
                },
                itemBuilder: (context, index) {
                  Widget result;

                  switch (index) {
                    case 0:
                      result = GreetingsItem(
                          scrollOffset: scrollOffset,
                          index: index,
                          controller: pageController);
                      break;
                    case 1:
                      result = MeetFlutterItem(
                        scrollOffset: scrollOffset,
                        index: index,
                        controller: pageController,
                      );
                      break;
                    case 2:
                      result = ProjectItem(
                        title: "Salesmatrix SWOT",
                        projectInfo: passages[PassageKeys.salesmatrix]
                            .toTextRich(linkColor: Colors.orange),
                        projectImage: AssetImage("assets/swot.png"),
                        projectLinks: {
                          "iOS":
                              "https://itunes.apple.com/tr/app/salesmatrix-swot/id1440022569?mt=8",
                          "Android":
                              "https://play.google.com/store/apps/details?id=com.salesmatrix.SalesmatrixSWOT&hl=en"
                        },
                        pointerTitle: "August 2018",
                        pointerColor: Colors.orange,
                        scrollOffset: scrollOffset,
                        index: index,
                        controller: pageController,
                      );
                      break;
                    case 3:
                      result = TitaniumItem(
                          scrollOffset: scrollOffset,
                          index: index,
                          controller: pageController);
                      break;
                    case 4:
                      result = ProjectItem(
                        title: "Mosoblgaz",
                        projectInfo: passages[PassageKeys.mosoblgaz]
                            .toTextRich(linkColor: Color(0xFF00456C)),
                        projectImage: AssetImage("assets/mosoblgaz.png"),
                        projectLinks: {
                          "iOS":
                              "https://itunes.apple.com/ru/app/licnyj-kabinet-castnogo-abonenta/id893083949?mt=8&uo=4",
                          "Android":
                              "https://play.google.com/store/apps/details?id=ru.mosoblgaz"
                        },
                        pointerTitle: "January 2016",
                        pointerColor: Color(0xFF00456C),
                        scrollOffset: scrollOffset,
                        index: index,
                        controller: pageController,
                      );
                      break;
                    case 5:
                      result = ProjectItem(
                        title: "Delikateska",
                        projectInfo: passages[PassageKeys.delikateska]
                            .toTextRich(linkColor: Color(0xFFB43641)),
                        projectImage: AssetImage("assets/delikateska.png"),
                        projectLinks: {
                          "iOS":
                              "https://itunes.apple.com/ru/app/delikateska.ru-internet-magazin/id500480133?&mt=8",
                          "Android":
                              "https://play.google.com/store/apps/details?id=com.rndelikateska"
                        },
                        pointerTitle: "December 2015",
                        pointerColor: Color(0xFFB43641),
                        scrollOffset: scrollOffset,
                        index: index,
                        controller: pageController,
                      );
                      break;
                    case 6:
                      result = KRItem(
                          scrollOffset: scrollOffset,
                          index: index,
                          controller: pageController);
                      break;
                    case 7:
                      result = ProjectItem(
                        title: "JW Planner",
                        projectInfo: passages[PassageKeys.jwplanner]
                            .toTextRich(linkColor: Color(0xFFE4500E)),
                        projectImage: AssetImage("assets/jwplanner.png"),
                        projectLinks: {
                          "iOS":
                              "https://itunes.apple.com/us/app/jw-planner-new/id1068099295?mt=8"
                        },
                        pointerTitle: "June 2014",
                        pointerColor: Color(0xFFE4500E),
                        scrollOffset: scrollOffset,
                        index: index,
                        controller: pageController,
                      );
                      break;
                    case 8:
                      result = CodingItem(
                          scrollOffset: scrollOffset,
                          index: index,
                          controller: pageController);
                      break;
                    case 9:
                      result = BabyItem(
                          scrollOffset: scrollOffset,
                          index: index,
                          controller: pageController);
                      break;
                    default:
                      result = Container();
                  }

                  return Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Container(height: padding.top),
                        Expanded(
                          child: result,
                        ),
                        Row(
                          children: timelineChildren,
                        ),
                        Container(
                          height: 8.0,
                        ),
                        Container(height: padding.bottom)
                      ],
                    ),
                  );
                }),
          ),
          Positioned(
              left: 0.0,
              right: 0.0,
              bottom: 16.0 + padding.bottom,
              child: Container(
                height: 1.0,
                color: Color(0xFF5C5C5C),
              )),
        ],
      ),
    );
  }
}

/// Helper class to size elements, paddings and fonts.

class LayoutHelper {
  Size screenSize;
  bool isTablet;
  Orientation orientation;

  double get defaultRaisedButtonHeight {
    if (isTablet) {
      return 50.0;
    } else {
      return 44.0;
    }
  }

  LayoutHelper({MediaQueryData query}) {
    this.screenSize = query.size;
    this.isTablet = query.size.shortestSide > 600.0;
    this.orientation = query.orientation;
  }

  /// Get max font size for plane body text.

  double getMaxFontSize() {
    if (isTablet) {
      return 28.0;
    } else {
      return 24.0;
    }
  }

  double getHugeTitleFontSize() {
    if (isTablet) {
      if (orientation == Orientation.landscape) {
        if (screenSize.shortestSide > 800.0) {
          return fontSize(k: 4.0);
        } else {
          return fontSize(k: 2.0);
        }
      } else {
        return fontSize(k: 4.0);
      }
    } else {
      return fontSize(k: 3.0);
    }
  }

  /// Get max icon/image size for Timeline items.

  double getMaxIconSize() {
    if (isTablet) {
      if (orientation == Orientation.landscape) {
        return 200.0;
      } else {
        return 250.0;
      }
    } else {
      if (screenSize.width < 960.0) {
        return 120.0;
      } else {
        return 150.0;
      }
    }
  }

  /// Get default translation for timeline subviews slide animation based on provided value, scroll offset and screen size.

  double getTranslation(double value, double offset) {
    return value * offset.abs() / screenSize.width;
  }

  double getTranslationX(double k, double offset) {
    return getTranslation(k * screenSize.width, offset);
  }

  double getTranslationY(double k, double offset) {
    return getTranslation(k * screenSize.height, offset);
  }

  /// Get font size based on privided screen size.
  /// NOTICE: k multiplier is used regardless max values.

  double fontSize({double k = 1.0}) {
    double result = screenSize.width * 5.0 / 100.0;

    if (result > getMaxFontSize()) {
      result = getMaxFontSize();
    }

    return k * result;
  }

  /// Return default icon/image size for Timeline item based on provided screen size.

  double iconSize() {
    double result = 0.4 * screenSize.shortestSide;

    if (result > getMaxIconSize()) {
      result = getMaxIconSize();
    }

    return result;
  }

  /// Get default paddings

  double get defaultPaddingValue {
    if (isTablet) {
      return 16.0;
    } else {
      return 8.0;
    }
  }

  double get defaultPaddingDoubleValue {
    return 2 * defaultPaddingValue;
  }

  double get defaultPaddingTrippleValue {
    return 2 * defaultPaddingValue;
  }

  EdgeInsets getDefaultPadding() {
    if (isTablet) {
      return EdgeInsets.symmetric(vertical: 16.0, horizontal: 48.0);
    } else {
      return EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0);
    }
  }
}

/// Class for keeping text paragraphs for app items.

class Passage {
  List<PassageSpan> spans;

  Passage(DocumentSnapshot doc) {
    List<dynamic> data = doc.data["spans"];
    var result = List<PassageSpan>();

    data.forEach((span) {
      result.add(PassageSpan(span));
    });

    spans = result;
  }

  Widget toTextRich({Color linkColor = Colors.blue, double kFontSize = 1.0}) {
    final textStyle = TextStyle(
        color: Colors.black,
        fontSize: layoutHelper.fontSize(k: kFontSize),
        height: 1.2);
    final linkStyle =
        TextStyle(color: linkColor, decoration: TextDecoration.underline);
    var children = List<TextSpan>();

    spans.forEach((span) {
      var recognizer;

      if (span.isLink) {
        recognizer = TapGestureRecognizer()
          ..onTap = () {
            if (span.link != null) {
              Launcher.openURL(span.link);
            }
          };
      }

      children.add(TextSpan(
          style: span.isLink ? linkStyle : null,
          text: span.text,
          recognizer: recognizer));
    });

    return Text.rich(TextSpan(style: textStyle, children: children));
  }
}

/// Unique keys for Passages

class PassageKeys {
  static final String greetings = "greetings";
  static final String flutter = "flutter";
  static final String salesmatrix = "salesmatrix";
  static final String titanium = "titanium";
  static final String mosoblgaz = "mosoblgaz";
  static final String delikateska = "delikateska";
  static final String kr = "kr";
  static final String jwplanner = "jwplanner";
  static final String firstSteps = "firstSteps";
  static final String aboutObjective1 = "aboutObjective1";
  static final String aboutObjective2 = "aboutObjective2";
  static final String aboutObjective3 = "aboutObjective3";
  static final String aboutObjective4 = "aboutObjective4";
  static final String aboutObjective5 = "aboutObjective5";
  static final String aboutObjective6 = "aboutObjective6";
  static final String aboutObjective7 = "aboutObjective7";
  static final String aboutObjective8 = "aboutObjective8";
  static final int count = 17;
}

/// Passage's element class

class PassageSpan {
  bool isLink;
  String text;
  String link;

  PassageSpan(Map<dynamic, dynamic> data) {
    this.isLink = data["isLink"];
    this.text = data["text"];
    this.link = data["link"];
  }
}
