import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'about_me.dart';
import 'launcher.dart';
import 'main.dart';

final double kMaxButtonWidth = 400.0;

/// Story item # 1 - Stateless app home screen.

class GreetingsItem extends TimelineItem {
  GreetingsItem({scrollOffset, index, controller})
      : super(scrollOffset: scrollOffset, index: index, controller: controller);

  @override
  Widget build(BuildContext context) {
    return MyStoryItem(
      scrollOffset: scrollOffset,
      pointerColor: Color(0xFF4265D0),
      pointerTitle: "Now",
      index: index,
      childBuilder: (context, offset, item) {
        return Container(
          //padding: layoutHelper.getDefaultPadding(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                    vertical: layoutHelper.defaultPaddingDoubleValue),
                child: Transform(
                  transform: Matrix4.identity()
                    ..translate(
                        offset, layoutHelper.getTranslationY(-0.5, offset)),
                  child: Container(
                    width: layoutHelper.iconSize(),
                    height: layoutHelper.iconSize(),
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/ava.png"),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(
                            layoutHelper.iconSize() / 2.0),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.4),
                              offset: Offset(0.0, 5.0),
                              blurRadius: 20.0,
                              spreadRadius: 0.0)
                        ]),
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.symmetric(
                      vertical: layoutHelper.defaultPaddingValue),
                  child: Center(
                    child: Container(
                        child: Material(
                      color: Colors.transparent,
                      child: DefaultTextStyle(
                        style: TextStyle(
                            fontFamily: "CaveatBrush",
                            fontSize: layoutHelper.getHugeTitleFontSize(),
                            fontWeight: FontWeight.bold,
                            color: item.pointerColor),
                        child: Row(
                          children: <Widget>[
                            Transform(
                                transform: Matrix4.identity()
                                  ..translate(layoutHelper.getTranslationX(
                                      -2.0, offset)),
                                child: Center(
                                  child: Text("H"),
                                )),
                            Container(width: 8.0),
                            Transform(
                              transform: Matrix4.identity()
                                ..translate(layoutHelper.getTranslationX(
                                    -1.85, offset)),
                              child: Center(
                                child: Text("e"),
                              ),
                            ),
                            Container(width: 8.0),
                            Transform(
                              transform: Matrix4.identity()
                                ..translate(
                                    layoutHelper.getTranslationX(-1.7, offset)),
                              child: Center(
                                child: Text("l"),
                              ),
                            ),
                            Container(width: 8.0),
                            Transform(
                              transform: Matrix4.identity()
                                ..translate(layoutHelper.getTranslationX(
                                    -1.55, offset)),
                              child: Center(
                                child: Text("l"),
                              ),
                            ),
                            Container(width: 8.0),
                            Transform(
                              transform: Matrix4.identity()
                                ..translate(layoutHelper.getTranslationX(
                                    -1.40, offset)),
                              child: Center(
                                child: Text("o"),
                              ),
                            ),
                            Container(width: 8.0),
                            Transform(
                              transform: Matrix4.identity()
                                ..translate(layoutHelper.getTranslationX(
                                    -1.25, offset)),
                              child: Center(
                                child: Text("!"),
                              ),
                            )
                          ],
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                        ),
                      ),
                    )),
                  )),
              Material(
                  color: Colors.transparent,
                  child: Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: Center(
                        child: passages[PassageKeys.greetings].toTextRich(),
                      ))),
              Container(
                  padding: layoutHelper.getDefaultPadding(),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: kMaxButtonWidth),
                    child: RaisedButton(
                      color: item.pointerColor,
                      child: Container(
                        height: layoutHelper.defaultRaisedButtonHeight,
                        child: Center(
                          child: Text("More About Me",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                      onPressed: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                fullscreenDialog: true,
                                builder: (context) => AboutMePage()));
                      },
                    ),
                  ))
            ],
          ),
        );
      },
    );
  }
}

/// Story item # 2 - Stateless view about Flutter

class MeetFlutterItem extends TimelineItem {
  MeetFlutterItem({scrollOffset, index, controller})
      : super(scrollOffset: scrollOffset, index: index, controller: controller);

  @override
  Widget build(BuildContext context) {
    return MyStoryItem(
        scrollOffset: scrollOffset,
        index: index,
        pointerColor: Colors.blue,
        pointerTitle: "October 2018",
        childBuilder: (context, offset, item) {
          final screenSize = MediaQuery.of(context).size;

          return Material(
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: Text("Becoming a Flutter developer...",
                          style: TextStyle(
                              color: item.pointerColor,
                              fontWeight: FontWeight.bold,
                              fontSize: layoutHelper.fontSize(k: 1.5)))),
                  Container(
                    padding: layoutHelper.getDefaultPadding(),
                    child: passages[PassageKeys.flutter].toTextRich(),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Transform(
                        transform: Matrix4.identity()
                          ..translate(
                              layoutHelper.getTranslationX(-1.5, offset)),
                        child: Icon(Icons.favorite,
                            size: 100.0, color: Colors.pink),
                      ),
                      Transform(
                        transform: Matrix4.identity()
                          ..translate(offset,
                              layoutHelper.getTranslationY(0.5, offset)),
                        child: Icon(Icons.add, size: 50.0, color: Colors.black),
                      ),
                      Transform(
                        transform: Matrix4.identity()
                          ..translate(
                              layoutHelper.getTranslationX(1.5, offset)),
                        child: FlutterLogo(size: 100.0),
                      ),
                    ],
                  )
                ],
              ));
        });
  }
}

/// Story item # 4 - Stateless view about working on Titanium Capital PTE

class TitaniumItem extends TimelineItem {
  TitaniumItem({scrollOffset, index, controller})
      : super(scrollOffset: scrollOffset, index: index, controller: controller);

  @override
  Widget build(BuildContext context) {
    return MyStoryItem(
        scrollOffset: scrollOffset,
        index: index,
        pointerColor: Color(0xFFfbc02d),
        pointerTitle: "January 2018",
        childBuilder: (context, offset, item) {
          return Material(
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    padding: layoutHelper.getDefaultPadding(),
                    child: Text("Getting new experience",
                        style: TextStyle(
                            color: item.pointerColor,
                            fontSize: layoutHelper.fontSize(k: 1.5),
                            fontWeight: FontWeight.bold,
                            height: 1.2)),
                  ),
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: DefaultTextStyle(
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: layoutHelper.fontSize(),
                            height: 1.2),
                        child: passages[PassageKeys.titanium]
                            .toTextRich(linkColor: item.pointerColor),
                      )),
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: kMaxButtonWidth),
                        child: RaisedButton(
                          color: item.pointerColor,
                          child: Container(
                            height: layoutHelper.defaultRaisedButtonHeight,
                            child: Center(
                              child: Text("Look at the website",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0)),
                          onPressed: () {
                            Launcher.openURL(
                                "https://titaniumcapitalforex.com");
                          },
                        ),
                      ))
                ],
              ));
        });
  }
}

/// Story item # 7 - Stateless view about working on KR Digital

class KRItem extends TimelineItem {
  KRItem({scrollOffset, index, controller})
      : super(scrollOffset: scrollOffset, index: index, controller: controller);

  @override
  Widget build(BuildContext context) {
    return MyStoryItem(
        scrollOffset: scrollOffset,
        index: index,
        pointerColor: Color(0xFF2AE9B9),
        pointerTitle: "November 2015",
        childBuilder: (context, offset, item) {
          return Material(
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: Center(
                        child: Image(
                            image: AssetImage("assets/kr.png"),
                            fit: BoxFit.contain,
                            width: layoutHelper.iconSize()),
                      )),
                  Container(
                    padding: layoutHelper.getDefaultPadding(),
                    child: Text("Meeting with KR Digital",
                        style: TextStyle(
                            color: item.pointerColor,
                            fontSize: layoutHelper.fontSize(k: 1.5),
                            fontWeight: FontWeight.bold,
                            height: 1.2)),
                  ),
                  Container(
                    padding: layoutHelper.getDefaultPadding(),
                    child: passages[PassageKeys.kr].toTextRich(),
                  ),
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: kMaxButtonWidth),
                        child: RaisedButton(
                          color: item.pointerColor,
                          child: Container(
                            height: layoutHelper.defaultRaisedButtonHeight,
                            child: Center(
                              child: Text("About company",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0)),
                          onPressed: () {
                            Launcher.openURL("https://kr.digital");
                          },
                        ),
                      ))
                ],
              ));
        });
  }
}

/// Story item # 9 - Stateless view about becoming a developer

class CodingItem extends TimelineItem {
  CodingItem({scrollOffset, index, controller})
      : super(scrollOffset: scrollOffset, index: index, controller: controller);

  @override
  Widget build(BuildContext context) {
    return MyStoryItem(
        scrollOffset: scrollOffset,
        index: index,
        pointerColor: Color(0xFF28384C),
        pointerTitle: "September 2007",
        childBuilder: (context, offset, item) {
          return Material(
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: Transform(
                        transform: Matrix4.identity()
                          ..translate(
                              layoutHelper.getTranslationX(-1.5, offset)),
                        child: Image(
                            image: AssetImage("assets/coding.png"),
                            fit: BoxFit.contain,
                            width: layoutHelper.iconSize()),
                      )),
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: Transform(
                        transform: Matrix4.identity()
                          ..translate(
                              layoutHelper.getTranslationX(1.5, offset)),
                        child: Text("First coding steps...",
                            style: TextStyle(
                                color: item.pointerColor,
                                fontSize: layoutHelper.fontSize(k: 1.5),
                                fontWeight: FontWeight.bold,
                                height: 1.2)),
                      )),
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: passages[PassageKeys.firstSteps].toTextRich())
                ],
              ));
        });
  }
}

/// Story item # 10 - Stateless view about birth date

class BabyItem extends TimelineItem {
  BabyItem({scrollOffset, index, controller})
      : super(scrollOffset: scrollOffset, index: index, controller: controller);

  @override
  Widget build(BuildContext context) {
    return MyStoryItem(
        scrollOffset: scrollOffset,
        index: index,
        pointerColor: Color(0xFF75AC37),
        pointerTitle: "August 1991",
        childBuilder: (context, offset, item) {
          final screenSize = MediaQuery.of(context).size;
          final scale = offset.abs() / screenSize.width + 1.0;

          return Material(
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: Transform(
                        transform: Matrix4.identity()
                          ..translate(offset,
                              layoutHelper.getTranslationY(-0.5, offset))
                          ..scale(scale, scale),
                        child: Image(
                            image: AssetImage("assets/baby.png"),
                            fit: BoxFit.contain),
                      )),
                  Container(
                      padding: layoutHelper.getDefaultPadding(),
                      child: Transform(
                        transform: Matrix4.identity()
                          ..translate(offset,
                              layoutHelper.getTranslationY(0.5, offset)),
                        child: Text("I was born",
                            style: TextStyle(
                                color: item.pointerColor,
                                fontSize: layoutHelper.fontSize(k: 2.5),
                                fontWeight: FontWeight.bold,
                                height: 1.2)),
                      ))
                ],
              ));
        });
  }
}

/// Common ancestor for timeline items about my projects

class ProjectItem extends TimelineItem {
  final String title;
  final AssetImage projectImage;
  final String pointerTitle;
  final Widget projectInfo;
  final Color pointerColor;
  final Map<String, String> projectLinks;

  ProjectItem(
      {this.title,
      this.projectInfo,
      this.projectImage,
      this.pointerTitle,
      this.pointerColor = Colors.blue,
      this.projectLinks,
      scrollOffset,
      index,
      controller})
      : super(scrollOffset: scrollOffset, index: index, controller: controller);

  @override
  Widget build(BuildContext context) {
    return MyStoryItem(
      scrollOffset: scrollOffset,
      index: index,
      pointerColor: pointerColor,
      pointerTitle: pointerTitle,
      childBuilder: (context, offset, item) {
        final screenSize = MediaQuery.of(context).size;
        var children = <Widget>[];

        projectLinks.forEach((key, value) {
          children.add(Expanded(
              flex: 1,
              child: Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: kMaxButtonWidth),
                  child: RaisedButton(
                    color: pointerColor,
                    child: Container(
                      height: layoutHelper.defaultRaisedButtonHeight,
                      child: Center(
                        child: Text(key,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      Launcher.openURL(value);
                    },
                  ),
                ),
              )));

          children.add(Container(
            width: 8.0,
          ));
        });

        children.removeLast();

        return Material(
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                    vertical: layoutHelper.defaultPaddingValue),
                child: Transform(
                    transform: Matrix4.identity()
                      ..translate(
                          offset, layoutHelper.getTranslationY(-0.5, offset)),
                    child: Center(
                      child: Container(
                        width: layoutHelper.iconSize(),
                        height: layoutHelper.iconSize(),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: projectImage, fit: BoxFit.cover),
                            borderRadius: BorderRadius.circular(
                                layoutHelper.iconSize() / 3.5),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.3),
                                  offset: Offset(0.0, 5.0),
                                  blurRadius: 10.0,
                                  spreadRadius: 0.0)
                            ]),
                      ),
                    )),
              ),
              Container(
                padding: layoutHelper.getDefaultPadding(),
                child: Text(title,
                    style: TextStyle(
                        color: pointerColor,
                        fontSize: layoutHelper.fontSize(k: 1.5),
                        fontWeight: FontWeight.bold,
                        height: 1.2)),
              ),
              Container(
                padding: layoutHelper.getDefaultPadding(),
                child: DefaultTextStyle(
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: layoutHelper.fontSize(),
                        height: 1.2),
                    child: projectInfo),
              ),
              Container(
                padding: layoutHelper.getDefaultPadding(),
                child: Row(
                  children: children,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

/// General wrapping widget for item pages in timeline (above timeline grid)

class TimelineItem extends StatelessWidget {
  final ValueNotifier<double> scrollOffset;
  final int index;
  final PageController controller;

  TimelineItem({this.scrollOffset, this.index, this.controller});

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

/// Story item page containing time pointer and child builder function (usually used to build timeline item)

class MyStoryItem extends StatefulWidget {
  final ValueListenable<double> scrollOffset;
  final Color pointerColor;
  final String pointerTitle;
  final Widget Function(BuildContext, double, MyStoryItem) childBuilder;
  final int index;

  MyStoryItem(
      {this.scrollOffset,
      this.pointerColor,
      this.pointerTitle,
      this.childBuilder,
      this.index});

  @override
  State<StatefulWidget> createState() => MyStoryItemState();
}

class MyStoryItemState extends State<MyStoryItem> {
  bool ignoreOffset = false;

  @override
  void didUpdateWidget(MyStoryItem oldWidget) {
    super.didUpdateWidget(oldWidget);
    ignoreOffset = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ValueListenableBuilder(
            valueListenable: widget.scrollOffset,
            builder: (context, value, child) {
              final sw = MediaQuery.of(context).size.width;
              double offset =
                  (widget.scrollOffset.value - sw * (widget.index).toDouble());
              var opacity = 1.0 - offset.abs() / sw;

              if (opacity < 0.0) {
                opacity = 0.0;
              } else if (opacity > 1.0) {
                opacity = 1.0;
              }

              if (ignoreOffset) {
                offset = 0.0;
                opacity = 1.0;
                ignoreOffset = false;
              }

              return Container(
                child: Column(
                  children: <Widget>[
                    Expanded(
                        child: Container(
                      child: Opacity(
                        opacity: opacity,
                        child: widget.childBuilder(context, offset, widget),
                      ),
                    )),
                    Container(
                      margin: EdgeInsets.only(top: 8.0),
                      padding:
                          EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                      decoration: BoxDecoration(
                        color: widget.pointerColor,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: Text(widget.pointerTitle ?? "null",
                            style: TextStyle(
                                color: Color(0xFFFFFFFF), fontSize: 15.0)),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/pointer.png"),
                              colorFilter: ColorFilter.mode(
                                  widget.pointerColor, BlendMode.srcATop))),
                      height: 8.0,
                      width: 16.0,
                    )
                  ],
                ),
              );
            }));
  }
}
