import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'launcher.dart';
import 'main.dart';

/// AboutMePage - resume summary

class AboutMePage extends StatelessWidget {
  static final skills = <Skill>[
    Skill(title: "Flutter Development", color: 0xFF4EADFF),
    Skill(title: "Dart Language", color: 0xFF4EADFF),
    Skill(title: "iOS Development", color: 0xFF9A4EFF),
    Skill(title: "Android Development", color: 0xFF73A446),
    Skill(title: "Swift", color: 0xFF9A4EFF),
    Skill(title: "Objective-C", color: 0xFF9A4EFF),
    Skill(title: "Kotlin", color: 0xFF73A446),
    Skill(title: "Java", color: 0xFF73A446),
    Skill(title: "Xcode", color: 0xFF9A4EFF),
    Skill(title: "Android Studio", color: 0xFF73A446),
    Skill(title: "Visual Studio", color: 0xFFE58700),
    Skill(title: "Xamarin", color: 0xFFE58700),
    Skill(title: "DJI SDK", color: 0xFF2250CC),
    Skill(title: "Twilio SDK", color: 0xFFE2430E),
    Skill(title: "Mapbox SDK", color: 0xFFE0A909),
    Skill(title: "HTML", color: 0xFFE11779),
    Skill(title: "CSS", color: 0xFFE11779),
    Skill(title: "JS", color: 0xFFE11779),
    Skill(title: "PHP", color: 0xFFE11779)
  ];

  final sections = <AboutMeSection>[
    AboutMeSection(
        header: AboutMeSectionHeader(
            title: "Objective", backgroundColor: Colors.blueAccent),
        body: Container(
            padding: EdgeInsets.all(layoutHelper.defaultPaddingDoubleValue),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective1]
                      .toTextRich(kFontSize: 0.75),
                ),
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective2]
                      .toTextRich(kFontSize: 0.75),
                ),
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective3]
                      .toTextRich(kFontSize: 0.75),
                ),
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective4]
                      .toTextRich(kFontSize: 0.75),
                ),
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective5]
                      .toTextRich(kFontSize: 0.75),
                ),
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective6].toTextRich(
                      linkColor: Color(0xFF06C278), kFontSize: 0.75),
                ),
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective7].toTextRich(
                      linkColor: Color(0xFF473174), kFontSize: 0.75),
                ),
                Container(
                  padding: EdgeInsets.all(layoutHelper.defaultPaddingValue),
                  child: passages[PassageKeys.aboutObjective8]
                      .toTextRich(kFontSize: 0.75),
                )
              ],
            )

            /*Text.rich(TextSpan(
              style: TextStyle(fontSize: 17.0, height: 1.2),
              children: [
                Container(
                  padding: EdgeInsets.all(5.0),
                  child: TextSpan(text: ""),
                ),
                TextSpan(text: ""),
                TextSpan(text: ""),
                TextSpan(text: ""),
                TextSpan(text: ""),
                TextSpan(text: ""),
                TextSpan(text: ""),
                TextSpan(text: ""),
              ])),*/
            )),
    AboutMeSection(
        header: AboutMeSectionHeader(
            title: "Skills", backgroundColor: Colors.redAccent),
        body: Container(
          padding: EdgeInsets.all(layoutHelper.defaultPaddingDoubleValue),
          child: Wrap(
              spacing: 8.0,
              runSpacing: 4.0,
              children: skills.map((skill) {
                return Chip(
                    label: ChipLabel(skill.title),
                    backgroundColor: Color(skill.color));
              }).toList()),
        )),
    AboutMeSection(
        header: AboutMeSectionHeader(
            title: "Languages", backgroundColor: Colors.orange),
        body: Container(
          padding: EdgeInsets.symmetric(
              vertical: layoutHelper.defaultPaddingDoubleValue),
          child: Column(
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.translate),
                title: Text("Russian (native)"),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
              ),
              Divider(height: 1.0),
              ListTile(
                leading: Icon(Icons.translate),
                title: Text("English (fluent)"),
                trailing: Icon(Icons.keyboard_arrow_right),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
                onTap: () {
                  Launcher.openURL("https://certs.duolingo.com/jyy8x8pb");
                },
              ),
              Divider(height: 1.0),
              ListTile(
                leading: Icon(Icons.translate),
                title: Text("Turkish (beginner)"),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
              ),
              Divider(height: 1.0),
              ListTile(
                leading: Icon(Icons.translate),
                title: Text("Chinese (beginner)"),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
              )
            ],
          ),
        )),
    AboutMeSection(
        header: AboutMeSectionHeader(
            title: "Education", backgroundColor: Colors.green),
        body: Container(
          padding: EdgeInsets.symmetric(
              vertical: layoutHelper.defaultPaddingDoubleValue),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: layoutHelper.defaultPaddingDoubleValue),
                  child: Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 400.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        color: Color(0xFFF6F6F6),
                        child: Container(
                          padding: EdgeInsets.all(
                              layoutHelper.defaultPaddingDoubleValue),
                          child: ListTile(
                            title: Text("High school (2004 - 2008)"),
                            subtitle:
                                Text("Gymnasium #16, Mytishchi, Moscow region"),
                          ),
                        ),
                      ),
                    ),
                  )),
              ListTile(
                title: Text("Developing applications for iOS"),
                trailing: Icon(Icons.attachment),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
                onTap: () {
                  Launcher.openURL(
                      "https://www.intuit.ru/verifydiplomas/100883867");
                },
              ),
              Divider(height: 1.0),
              ListTile(
                title: Text("Programming with Objective-C"),
                trailing: Icon(Icons.attachment),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
                onTap: () {
                  Launcher.openURL(
                      "https://www.intuit.ru/verifydiplomas/100765726");
                },
              )
            ],
          ),
        )),
    AboutMeSection(
        header: AboutMeSectionHeader(
            title: "References", backgroundColor: Colors.purple),
        body: Container(
          padding: EdgeInsets.symmetric(
              vertical: layoutHelper.defaultPaddingDoubleValue),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
                child: Text("Ivan Ryndyuk",
                    style:
                        TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("vanek.jw@gmail.com"),
                subtitle: Text("email"),
                trailing: Icon(Icons.mail),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
                onTap: () {
                  Launcher.openURL("mailto:vanek.jw@gmail.com");
                },
              ),
              Divider(height: 1.0),
              ListTile(
                title: Text("Antalya, Turkey"),
                subtitle: Text("location"),
                trailing: Icon(Icons.location_on),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
                onTap: () {
                  Launcher.openURL(
                      "https://www.google.com/maps/place/Antalya/");
                },
              ),
              Divider(height: 1.0),
              ListTile(
                title: Text("LinkedIn"),
                subtitle: Text("profile"),
                trailing: Icon(Icons.person),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: layoutHelper.defaultPaddingDoubleValue),
                onTap: () {
                  Launcher.openURL("https://www.linkedin.com/in/ivanryndyuk");
                },
              )
            ],
          ),
        ))
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Material(
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              Container(
                height: 70.0,
                padding: EdgeInsets.only(
                    top: layoutHelper.defaultPaddingValue,
                    bottom: layoutHelper.defaultPaddingValue,
                    left: layoutHelper.defaultPaddingDoubleValue),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("About me",
                            style: TextStyle(
                                color: Color(0xFF333333),
                                fontSize: 40.0,
                                fontWeight: FontWeight.bold))),
                    Container(
                      width: 60.0,
                      height: 60.0,
                      child: Center(
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          child: Center(
                              child: Icon(Icons.close, color: Colors.black)),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                  child: ListView.builder(
                      itemCount: sections.length,
                      itemBuilder: (context, index) {
                        final section = sections[index];

                        return StickyHeader(
                          header: section.header,
                          content: section.body,
                        );
                      }))
            ],
          ),
        ),
      ),
    );
  }
}

/// Class to keep sections header and body in order to get them conveniently

class AboutMeSection {
  final Widget header;
  final Widget body;

  AboutMeSection({this.header, this.body});
}

/// Reusable widget for section header

class AboutMeSectionHeader extends StatelessWidget {
  final String title;
  final Color titleColor;
  final Color backgroundColor;

  AboutMeSectionHeader(
      {@required this.title,
      this.titleColor = Colors.white,
      this.backgroundColor = Colors.blue});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 40.0,
        color: backgroundColor,
        child: Center(
          child: Text(title,
              style: TextStyle(
                  color: titleColor,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold)),
        ));
  }
}

/// Class to keep info about skills

class Skill {
  final int color;
  final String title;

  Skill({this.title, this.color});
}

/// Widget with default behavior for skills chips.

class ChipLabel extends StatelessWidget {
  final title;

  ChipLabel(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(title,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold));
  }
}
